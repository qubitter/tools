#!/bin/sh

# check if we have rg installed, if not, we install it
if ! command -v rg &> /dev/null
then
    # check if we already have it unpacked
    if [ -f ./bins/rg ]; then
      RG_BIN=$(realpath ./bins/rg)
    else
      echo "rg could not be found, installing it via tarball at ./bins/rg.tar.gz"
      tar -xzf ./bins/rg.tar.gz -C ./bins
      RG_BIN=$(realpath ./bins/rg)
    fi
else
    RG_BIN=$(command -v rg)
fi
echo "Using rg at $RG_BIN"

ANOMALOUS_STRINGS='(^|\s)(nc|netcat|curl|wget|ftp|ssh|scp|sftp|telnet|ping|nmap|ncat|openssl|socat)(\s)'
# paths to search
SEARCH_PATHS='/tmp /var/ /etc/init.d/ /etc/rc.d/ /etc/rc.local /etc/cron.* /etc/crontab'
for SEARCH_PATH in $SEARCH_PATHS; do
  $RG_BIN $ANOMALOUS_STRINGS $SEARCH_PATH 2>/dev/null
done
