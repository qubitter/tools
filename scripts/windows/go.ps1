# Path to Sysinternals
$SysinternalsPath = "C:\Program Files\sysinternals"

Start-Process powershell.exe -ArgumentList "-File .\install-sysinternals.ps1"


# Open Sysinternals tools
Start-Process -FilePath "$SysinternalsPath\procmon64.exe"
Start-Process -FilePath "$SysinternalsPath\tcpview64.exe"
Start-Process -FilePath "$SysinternalsPath\autoruns64.exe"
Start-Process -FilePath "$SysinternalsPath\procexp64.exe"


# Run the rest
# get info on system
Start-Process powershell.exe -ArgumentList "-File .\information.bat"

# run sfc /scannow
Start-Process powershell.exe -ArgumentList "-File .\sfc.ps1"

# windows defender script
Start-Process powershell.exe -ArgumentList "-File .\sos-windowsdefenderhardening.ps1"

# lots of hardening
Start-Process powershell.exe -ArgumentList "-File .\first-hour.ps1"

# get and run winpeas
Start-Process powershell.exe -ArgumentList "-File .\invoke-winpeas.ps1"
